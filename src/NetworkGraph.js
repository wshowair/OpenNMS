import React, { useEffect, useRef } from 'react'
import { arrayOf, string, shape, oneOf } from 'prop-types'
import styled from 'styled-components'
import { select } from 'd3'
import { forceSimulation, forceLink, forceManyBody, forceCenter } from 'd3-force'

const Container = styled.div``;
const Graph = styled.svg`
  width: 100%;
  height: 100%;
  circle {
    r: 12px;
  }
  line {
    stroke: #ccc;
    stroke-width: 1px;
  }
`;

const NetworkGraph = (props) => {

  const svgRef = useRef(null)

  const simulationRef = useRef(
    forceSimulation()
    .force("link",
      forceLink()
        .id(d => d.id)
        .strength(1)
        .distance(100)
    )
    .force("charge", forceManyBody().strength(-400))
    .force("center", forceCenter(0, 0))
  )

  
  useEffect(() => {
    let svg = select(svgRef.current)
    const {width, height} = svg.node().getBoundingClientRect()
    svg = svg.html("")
            .attr("viewBox", [-width / 2, -height / 2, width, height]);

    // Initialize the nodes
    const svgCircles = svg.append('g')
      .selectAll('circle')
      .data(props.data.vertices)
      .join(
        enter => enter.append('circle')
                      .attr('fill', vertex => vertex.type === 'alarm' ? 'red': 'green')
                      .attr("id", d => d.id),
        exit => exit.remove()
      )

    // Initialize the links
    const svgLinks = svg.append('g')
      .selectAll("line")
      .data(props.data.edges)
      .join(
        enter => enter.append('line'),
        exit => exit.remove(),
      )

    const ticked = () => {
      svgLinks
        .attr("x1", d => d.source.x)
        .attr("y1", d => d.source.y)
        .attr("x2", d => d.target.x)
        .attr("y2", d => d.target.y)

      svgCircles
        .attr("cx", d => d.x)
        .attr("cy", d => d.y)
        
    }


    const simulation = simulationRef.current

    simulation.on('tick', ticked)
    simulation.nodes(props.data.vertices);
    simulation.force("link").links(props.data.edges);
    simulation.alpha(1).restart();

    return () => {
      simulation.stop()
    }

  }, [props.data])


  return (
    <Container className={props.className}>
      <Graph ref={svgRef}/>
    </Container>
  )
}

NetworkGraph.propTypes = {
  className: string,
  data: shape({
    verticies: arrayOf(shape({
      id: string,
      label: string,
      type: oneOf(['node', 'alaram']),
    })),
    edges: arrayOf(shape({
      id: string,
      label: string,
      type: oneOf(['link']),
      source: string,
      target: string,

    }))
  })
}

NetworkGraph.defaultProps = {
  data: {
    vertices: [],
    edges: [],
  }
}
export default NetworkGraph;
