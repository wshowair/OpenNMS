import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import NetworkInput from './NetworkInput';

test('parses valid network input string as json', () => {
  const parseDataSuccessMock = jest.fn()
  const { getByText, getByRole } = render(<NetworkInput onParseDataSuccess={parseDataSuccessMock}/>);
  fireEvent.change(getByRole("textbox"), {
    target: {
    value: `{
      "vertices":[{"id":"n1"}],
      "edges": [{"source_id": "n1","target_id": "n2"}]
		}`
  }});

  fireEvent.click(getByText("Enter"))
  expect(parseDataSuccessMock).toHaveBeenCalledWith({
    vertices: [{id: 'n1'}],
    edges: [{source: 'n1', target: 'n2'}]
  })
});

test('informs user about invalid network input string', () => {
  const { getByText, getByRole } = render(<NetworkInput/>);
  fireEvent.change(getByRole("textbox"), {
    target: {
    value: `{
      "vertices":[{"id":"n1"
		}`
  }});

  expect(getByText("Invalid network data")).toBeInTheDocument();
})
