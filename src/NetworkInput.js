import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';

const Button = styled.button`
  background-color: rgb(140, 191, 60);
  color: white;
  padding: 10px;
  border: none;
`;

const FormGroup = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
`;

const Error = styled.p`
  color: red;
  font-size: 0.8rem;
`;

const NetworkInput = (props) => {
  const [userInput, setUserInput] = useState('')
  const [error, setError] = useState('')

  const handleNetworkInput = (event) => {
    event.preventDefault()
    if(error){
      return
    }
    const {vertices, edges } = JSON.parse(userInput)
    const mappedEdges = edges.map(edge => {
      const {source_id, target_id, ...rest} = edge
      return {...rest, source: source_id, target: target_id}
    })
    const mappedInput = { vertices, edges:mappedEdges }
    props.onParseDataSuccess(mappedInput)
  }

  const handleInputChange = event => {
    const rawInput = event.target.value
    
    setUserInput(rawInput)
    try {
      JSON.parse(rawInput)
      setError('')
    } catch (error) {
      setError( rawInput ? 'Invalid network data': '')
    }
  }
  
  return (
    <form onSubmit={handleNetworkInput} className={props.className}>
      <FormGroup>
        <label htmlFor="network-input">Network data</label>
        <textarea 
          id="network-input"
          rows="40" 
          value={userInput} 
          onChange={handleInputChange}/>
        {error && <Error>{error}</Error>}
      </FormGroup>

      <Button type="submit">Enter</Button>
    </form>
  )
}

NetworkInput.propTypes = {
  onParseDataSuccess: PropTypes.func,
  onParseDataFailure: PropTypes.func,
  className: PropTypes.string,
}

NetworkInput.defaultProps = {
  onParseDataSuccess: () => {},
  onParseDataFailure: () => {}
}
export default NetworkInput;
