import React, { useState } from 'react'
import styled from 'styled-components'
import NetworkInputComp from './NetworkInput'
import NetworkGraphComp from './NetworkGraph';

const NetworkInput = styled(NetworkInputComp)`
  flex: 1 1 0;
  margin-right: 5px;
`;
const NetworkGraph = styled(NetworkGraphComp)`
  flex: 1 1 0;
  margin-left: 5px;
`;

const Layout = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const App = () => {
  const [networkData, setNetworkData] = useState({vertices: [], edges: []})
  return (
    <>
      <h1>OpenNMS</h1>
      <Layout>
        <NetworkInput onParseDataSuccess={data => setNetworkData(data)} />
        <NetworkGraph data={networkData} />
      </Layout>
    </>
  )
}

export default App;
