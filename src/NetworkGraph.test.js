import React from 'react';
import { render } from '@testing-library/react';
import NetworkGraph from './NetworkGraph';
import { twoNodesAndOneAlarm } from './testing/data'

test('Network Graph Snapshot', () => {
  const { asFragment } = render(<NetworkGraph />);
  expect(asFragment()).toMatchSnapshot();
});

test('differentiates vertices by type: alaram versus node ', () => {
  const {  container } = render(<NetworkGraph  data={twoNodesAndOneAlarm}/>);

  expect(container.querySelectorAll('circle[fill="red"]').length).toEqual(1)
  expect(container.querySelectorAll('circle[fill="green"]').length).toEqual(2)
})