This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

* The project uses d3 force layout to render the network data. 
* React Testing library is used to unit test the code without focusing on implementation details.
* Styled Components is used to develop css code.
* If you would like to see an angular implementation for the same project, please checkout `angular` branch in this repository.

## Installation
Run `npm install` to download all the dependecy modules.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

